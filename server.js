var express = require('express'),
 app = express(),
 port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
 res.sendFile(path.join(__dirname, 'index.html'));
});

app.post('/', function(req, res) {
 res.send('Hemos recibido su peticion cambiada');
});

app.get('/clientes', function(req, res) {
 res.sendFile(path.join(__dirname, 'clientesGET.json'));
});

app.post('/clientes', function(req, res) {
 res.sendFile(path.join(__dirname, 'clientesPOST.json'));
});

app.put('/clientes', function(req, res) {
 res.sendFile(path.join(__dirname, 'clientesPUT.json'));
});

app.delete('/clientes', function(req, res) {
 res.sendFile(path.join(__dirname, 'clientesDELETE.json'));
});

app.get('/clientes/:idcliente', function(req, res) {
 res.send('Aqui tiene al cliente con ID ' + req.params.idcliente);
});
